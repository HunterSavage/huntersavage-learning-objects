package tests;

import com.browserstack.BrowserStackTestNGTest;
import com.browserstack.localTest;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.*;

public class registrationTests extends localTest {

    /*
    I have created this example with a BrowserStack class so that the tests can be executed through BrowserStack. To do
    this you must change the username and password to yours in the registration.conf.json file and change the 'extends'
    feature to point at BrowserStackTestNGTest. This will run the 2 tests on  4 browsers (Edge, Firefox, Chrome, Safari)
    and to verify cross-browser functionality.

    To run simply navigate to project through command line and run 'mvn test -P registration-tests' OR to run locally keep
    localTest as extension and run through IDE play button.
     */

    /*
    For taskOne I have disabled the submit button as I do not want to actually register my e-mail every time the test is
    run. In a working case on a local or client website I would set an email and create a random number generator using
    the Java random class. The input would be email+(random int generated each test)@gmail.com and the same random numbers
    can be used for a username to ensure a very high chance of a unique email/ username every time.

    The test simply goes through and fills out every field on the page, however no asserts are made during the test as I
    would depend on missing elements causing a failure to alert issue.

    The profile picture element can be disabled if test is failing on your machine, the issue is likely from a bad file
    path that is submitting, this can be changed to any image or just changed to match your machine.
     */
    @Test
    public void taskOne(){
        registrationObjects obj = PageFactory.initElements(driver, registrationObjects.class);
        obj.allFields();
        obj.strongPassword();
        //obj.clickSubmit();
    }

    /*
    Same for this one as above, submit is disabled but I am still able to verify each password strength type by entering
    password and asserting the text to expected results.

    After each entry, the password fields are cleared. On the last one (strong password) it will not clear and submit
    this password.
     */
    @Test
    public void taskTwo(){
        registrationObjects obj = PageFactory.initElements(driver, registrationObjects.class);
        obj.requiredFields();
        obj.invalidPassword();
        obj.clearPassword();
        obj.weakPassword();
        obj.clearPassword();
        obj.strongPassword();
        //obj.clickSubmit();
    }
}
