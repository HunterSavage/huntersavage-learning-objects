package tests;

import junit.framework.Assert;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class registrationObjects {
    final WebDriver driver;
    public registrationObjects(WebDriver driver) {
        this.driver = driver;
    }

    //First Name field
    @FindBy(how = How.ID, using = "name_3_firstname")
    private WebElement firstName;

    //Last Name Field
    @FindBy(how = How.ID, using = "name_3_lastname")
    private WebElement lastName;

    //Marital Status - Single
    @FindBy(how = How.XPATH, using = "//input[@value = 'single']")
    private WebElement maritalSingle;

    //Marital Status - Married
    @FindBy(how = How.XPATH, using = "//input[@value = 'married']")
    private WebElement maritalMarried;

    //Marital Status - Divorced
    @FindBy(how = How.XPATH, using = "//input[@value = 'divorced']")
    private WebElement maritalDivorced;

    //Hobby - Dance
    @FindBy(how = How.XPATH, using = "//input[@value = 'dance']")
    private WebElement hobbyDance;

    //Hobby - Reading
    @FindBy(how = How.XPATH, using = "//input[@value = 'reading']")
    private WebElement hobbyReading;

    //Hobby - Cricket
    @FindBy(how = How.XPATH, using = "//input[@value = 'cricket']")
    private WebElement hobbyCricket;

    //Country Dropdown
    @FindBy(how = How.ID, using = "dropdown_7")
    private WebElement countryDropdown;

    //Date of Birth - Month
    @FindBy(how = How.ID, using = "mm_date_8")
    private WebElement dobMonth;

    //Date of Birth - Day
    @FindBy(how = How.ID, using = "dd_date_8")
    private WebElement dobDay;

    //Date of Birth - Year
    @FindBy(how = How.ID, using = "yy_date_8")
    private WebElement dobYear;

    //Phone Number
    @FindBy(how = How.ID, using = "phone_9")
    private WebElement phoneNumber;

    //Username
    @FindBy(how = How.ID, using = "username")
    private WebElement username;

    //E-Mail
    @FindBy(how = How.ID, using = "email_1")
    private WebElement email;

    //Profile Picture
    @FindBy(how = How.ID, using = "profile_pic_10")
    private WebElement profilePicture;

    //About Yourself
    @FindBy(how = How.ID, using = "description")
    private WebElement aboutYourself;

    //Password One
    @FindBy(how = How.ID, using = "password_2")
    private WebElement passwordOne;

    //Password Two
    @FindBy(how = How.ID, using = "confirm_password_password_2")
    private WebElement passwordTwo;

    //Password Error Text
    @FindBy(how = How.XPATH, using = "//span[@class = 'legend error']")
    private WebElement passwordError;

    //Password Strength Indicator
    @FindBy(how = How.ID, using = "piereg_passwordStrength")
    private WebElement passwordStrength;

    //Submit
    @FindBy(how = How.NAME, using = "pie_submit")
    private WebElement submit;

    public void allFields(){
        firstName.sendKeys("Hunter");
        lastName.sendKeys("Savage");
        maritalSingle.click();
        hobbyDance.click();
        Select dropCountry = new Select(countryDropdown);
        dropCountry.selectByValue("United States");
        Select dropMonth = new Select(dobMonth);
        dropMonth.selectByValue("3");
        Select dropDay = new Select(dobDay);
        dropDay.selectByValue("11");
        Select dropYear = new Select(dobYear);
        dropYear.selectByValue("1991");
        phoneNumber.sendKeys("123-456-7890");
        username.sendKeys("HunterSavage");
        email.sendKeys("Hunter.j.savage@gmail.com");
        //profilePicture.sendKeys("C:\\Users\\hunter.savage\\Desktop\\HunterSavage-learning-registrationObjects-test\\billgates.jpg");
        aboutYourself.sendKeys("Some stuff about myself for this field test.");
    }

    public void requiredFields(){
        firstName.sendKeys("Required Field");
        lastName.sendKeys("Required Field");
        hobbyDance.click();
        phoneNumber.sendKeys("123-123-1234");
        username.sendKeys("Required Field");
        email.sendKeys("required@field.com");
    }

    public void invalidPassword(){
        passwordOne.sendKeys("123");
        passwordTwo.sendKeys("123");
        Assert.assertEquals("* Minimum 10 Digits starting with Country Code", passwordError.getText());
    }

    public void weakPassword(){
        passwordOne.sendKeys("!@#!@#!@#");
        passwordTwo.sendKeys("!@#!@#!@#");
        Assert.assertEquals("Weak", passwordStrength.getText());
    }

    public void strongPassword(){
        passwordOne.sendKeys("!@#!@#!@#!@#");
        passwordTwo.sendKeys("!@#!@#!@#!@#");
        Assert.assertEquals("Strong", passwordStrength.getText());
    }

    public void clearPassword(){
        passwordOne.clear();
        passwordTwo.clear();
    }

    public void clickSubmit(){
        submit.click();
    }
}
